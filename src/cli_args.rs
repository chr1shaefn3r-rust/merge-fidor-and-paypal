use structopt::StructOpt;
use std::path::PathBuf;

#[derive(StructOpt)]
pub struct Cli {
    #[structopt(parse(from_os_str), default_value = "./fidor.csv")]
    pub path_to_fidor_csv: PathBuf,
    #[structopt(parse(from_os_str), default_value = "./paypal.csv")]
    pub path_to_paypal_csv: PathBuf
}
