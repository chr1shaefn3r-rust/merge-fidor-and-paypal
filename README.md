# merge-fidor-and-paypal

>> Small cli tool to merge csv exports from fidor and paypal

## Linux build
```bash
$ cargo build --release
$ strip target/release/merge-fidor-and-paypal
```

