use structopt::StructOpt;
use std::fs;

use csv::ReaderBuilder;
use serde::Deserialize;
mod cli_args;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct FidorTransaction {
    datum: String,
    beschreibung: String,
    beschreibung2: String,
    wert: String,
}

fn main() {
    let args = cli_args::Cli::from_args();
    println!("Called with fidorCSV: {:#?} and paypalCSV: {:#?}", args.path_to_fidor_csv, args.path_to_paypal_csv);

    let fidor_transactions = fs::read_to_string(args.path_to_fidor_csv).unwrap_or_else(|error| {
        eprintln!(
            "Couldn't read fidor transactions. Error: '{}'", error
        );
        std::process::exit(exitcode::NOINPUT);
    });

    let mut reader = ReaderBuilder::new().delimiter(b';').from_reader(fidor_transactions.as_bytes());
    for result in reader.deserialize::<FidorTransaction>() {
        println!("{:?}", result.unwrap());
    }
}
